#ifndef CW3_THE_PLAYLIST
#define CW3_THE_PLAYLIST

#include "the_button.h"

#include <vector>
#include <string>
#include <random>

using namespace std;

/**
 * @brief Playlist containing metadata about videos
 */
class ThePlaylist {
private:
    vector<TheButtonInfo> buttonInfos;

public:
    /**
     * @brief Construct a new Playlist based on the videos in a directory
     */
    ThePlaylist(const vector<TheButtonInfo>& videos);

    /**
     * @brief Delete the playlist and all its contents
     */
    ~ThePlaylist();

    /**
     * @brief Gets the button info for the videos
     */
    vector<TheButtonInfo>& getVideoInfo();

    /**
     * @brief Gets the currently playing video
     */
    TheButtonInfo* getCurrentVideo();

signals:
    void jumpTo(TheButtonInfo*);
};

#endif