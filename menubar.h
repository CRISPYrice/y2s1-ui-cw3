#ifndef MENUBAR_H
#define MENUBAR_H

#include <QWidget>
#include "the_button.h"

class QFileDialog;
class QPushButton;
class QHBoxLayout;
class QUrl;
class QString;

/*class for menu bar, currently implements an open file feature.
Has potential for future widget additions.
*/
class Menubar : public QWidget
{
    Q_OBJECT
public:
    Menubar(); //constructor
signals:
    void openNewVideoURL(TheButtonInfo*); //signal for emitting chosen file info
private:
    void createWidgets(); //dyanimically creates child widgets
    void arrangeWidgets(); //arranges widgets into a QHBoxLayout
    void connectWidgets(); //connects open file button to openFile signal

    QPushButton* openFileBtn;
    QString* selectedFile;
    TheButtonInfo* newVideoButtonInfo;

private slots:
    void openFile(); //handles file selection and emission
};

#endif // MENUBAR_H
