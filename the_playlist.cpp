#include "the_playlist.h"
#include "the_playlist_area.h"
#include "the_player.h"

#include <random>
#include <algorithm>

using namespace std;

ThePlaylist::ThePlaylist(const vector<TheButtonInfo>& videos) {
    buttonInfos = vector<TheButtonInfo>(videos);
}

ThePlaylist::~ThePlaylist() {
    // Nothing to destruct
}

vector<TheButtonInfo>& ThePlaylist::getVideoInfo() {
    return buttonInfos;
}

TheButtonInfo* ThePlaylist::getCurrentVideo() {
    return Q_NULLPTR;
}