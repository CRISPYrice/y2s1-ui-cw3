#include "previous_button.h"

#include <QPushButton>
#include <QResizeEvent>

PreviousButton::PreviousButton() : QPushButton() {

    setMaximumSize(QSize(50, 50));
    setMinimumSize(QSize(30, 30));
    setIcon(QIcon(":/icons/prev.svg"));
    connect(this, SIGNAL(released()), this, SLOT(clicked()));
}

void PreviousButton::resizeEvent(QResizeEvent* event) {
    //shrink to smaller dimension
    int target;
    if (width()>height())
        target=height();
    else
        target=width();

    //keep button square
    resize(target, target);
    setIconSize(QSize(target*.8, target*.8));
}

void PreviousButton::clicked() {
    emit previousVideo();
}