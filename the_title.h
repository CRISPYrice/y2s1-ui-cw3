#ifndef THE_TITLE_H
#define THE_TITLE_H

#include <QObject>
#include <QLabel>
#include "the_button.h"
#include <vector>

using namespace std;

class the_title : public QLabel
{
    Q_OBJECT

private:
    vector<TheButtonInfo>* infos;
    vector<TheButton*>* buttons;
    int pos;

public:
    the_title(){
        ;
    }
    void set_the_title(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i); //main title set up
public slots:
    void jumpTo (TheButtonInfo* button); //title changer
signals:
};

#endif // THE_TITLE_H
