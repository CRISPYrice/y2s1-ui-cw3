#ifndef THE_RESPONSIVE_LAYOUT_H
#define THE_RESPONSIVE_LAYOUT_H

#include "the_video_area.h"
#include "the_playlist.h"
#include "the_playlist_area.h"

#include <QLayout>
#include <QLayoutItem>
#include <QHBoxLayout>
#include <QRect>
#include <QList>

/**
 * @brief A custom implemented responsive layout for our needs
 */
class TheResponsiveLayout : public QLayout {
private:
    // Holds the items in the layout
    QList<QLayoutItem*> list_;

    // Video Player
    TheVideoArea* videoArea;

    // Buttons
    ThePlaylistArea* playlistArea;
public:
    TheResponsiveLayout(vector<TheButtonInfo>& videos, QWidget& window);
    ~TheResponsiveLayout();

    /* INHERITED FUNCTIONS */

    /**
     * @brief Standard functions for a QLayout
     */
    void setGeometry(const QRect &rect);

    /**
     * @brief Adds an item to the layout's list of elements
     *
     * @param item The item to add
     */
    void addItem(QLayoutItem *item);
    /**
     * @brief Gets the minimum size
     *
     * @return QSize
     */
    QSize sizeHint() const;
    /**
     * @brief Gets the minimum size
     *
     * @return @c QSize
     */
    QSize minimumSize() const;
    /**
     * @brief Gets the number of items in the layout's element list
     *
     * @return @c int
     */
    int count() const;
    /**
     * @brief Gets the item at a particular index
     *
     * @return @c QLayoutItem*
     */
    QLayoutItem *itemAt(int) const;
    /**
     * @brief Gets and removes the item at a particular index
     *
     * @return @c QLayoutItem*
     */
    QLayoutItem *takeAt(int);
};

#endif