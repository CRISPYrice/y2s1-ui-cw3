QT += core gui widgets multimedia multimediawidgets

CONFIG += c++11
CONFIG += debug


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        previous_button.cpp \
        next_button.cpp \
        shuffle_button.cpp \
        fullscreen_button.cpp \
        menubar.cpp \
        pause_button.cpp \
        the_button.cpp \
        the_player.cpp \
        tomeo.cpp \
        volume_control.cpp \
        the_video_area.cpp \
        the_responsive_layout.cpp \
        the_playlist.cpp \
        the_playlist_area.cpp \
        the_videoslider.cpp \
        the_title.cpp


HEADERS += \
    previous_button.h \
    next_button.h \
    shuffle_button.h \
    fullscreen_button.h \
    menubar.h \
    pause_button.h \
    the_button.h \
    the_player.h \
    volume_control.h \
    the_video_area.h \
    the_responsive_layout.h \
    the_playlist.h \
    the_playlist_area.h \
    the_videoslider.h \
    volume_control.h \
    the_title.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=

RESOURCES += \
    application.qrc

