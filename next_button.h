#ifndef NEXT_BUTTON_H
#define NEXT_BUTTON_H

#include <QPushButton>

class NextButton : public QPushButton {
Q_OBJECT
private:
public:
    NextButton();

    int heightForWidth(int w) const { return w; }

    void resizeEvent(QResizeEvent *event);

public slots:
    void clicked();

signals:
    void nextVideo();
};

#endif // NEXT_BUTTON_H