#include "the_title.h"
#include "the_button.h"
#include <QLabel>
#include <QUrl>


//creates the main player title
void the_title::set_the_title(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i){
    buttons = b;
    infos = i;
    jumpTo(buttons -> at(0) -> info);
}

//changes title
void the_title::jumpTo (TheButtonInfo* button) {
    QString title_full =  (* button -> url).fileName();
    pos = title_full.lastIndexOf(QChar('.'));
    setText("Title: "+ title_full.left(pos));

}
