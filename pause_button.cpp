#include "pause_button.h"
#include <QApplication>
PauseButton::PauseButton() : QPushButton(){
    //load icons from resources
    plIcon = QIcon(":/icons/playbutton.png");
    paIcon = QIcon(":/icons/pausebutton.png");

    setMinimumSize(QSize(30,30));
    setMaximumSize(QSize(50,50));

    setIcon(paIcon);

    connect(this, SIGNAL(released()), this, SLOT(clicked()) );
}
void PauseButton::resetState(){
    playState=true;
    setIcon(paIcon);
}
void PauseButton::clicked(){
    playState=!playState;
    if(playState==true)
        setIcon(paIcon);
    else
        setIcon(plIcon);
    emit togglePlayback();
}
void PauseButton::resizeEvent(QResizeEvent *event){
    //shrink to smaller dimension
    int target;
    if(width()>height())
        target=height();
    else
        target=width();

    //keep button square
    resize(target,target);
    setIconSize(QSize(target*.8,target*.8));
}
