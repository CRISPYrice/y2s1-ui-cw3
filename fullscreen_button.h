#ifndef FULLSCREENBUTTON_H
#define FULLSCREENBUTTON_H

#include <QPushButton>

class FullscreenButton : public QPushButton{
Q_OBJECT
public:
    FullscreenButton():QPushButton(){

        setMaximumSize(QSize(50,50));
        setMinimumSize(QSize(30,30));
        setIcon(QIcon(":/icons/fullscreen.png"));
        connect(this, SIGNAL(released()), this, SLOT(clicked()));
    }
    int heightForWidth(int w) const{
        return w;
    }
    //implement to keep button square
    void resizeEvent(QResizeEvent *event);
private:
    bool screenState=false;
public slots:
    void clicked();
signals:
    //for manupulating the window
    void fullscreenOn();
    void fullscreenOff();
};

#endif // FULLSCREENBUTTON_H
