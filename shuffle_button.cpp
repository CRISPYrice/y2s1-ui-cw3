#include "shuffle_button.h"

#include <QSize>

ShuffleButton::ShuffleButton() : QPushButton() {
    setMaximumSize(QSize(50, 50));
    setMinimumSize(QSize(30, 30));
    setIcon(QIcon(":/icons/shuffle.svg"));
    connect(this, SIGNAL(released()), this, SLOT(clicked()));
}

void ShuffleButton::clicked() {
    emit shuffleVideos();
}

void ShuffleButton::resizeEvent(QResizeEvent *event) {
    //shrink to smaller dimension
    int target;
    if (width()>height())
        target=height();
    else
        target=width();

    //keep button square
    resize(target, target);
    setIconSize(QSize(target*.8, target*.8));
}