#ifndef THE_VIDEOSLIDER_H
#define THE_VIDEOSLIDER_H

#include <QString>
#include <QWidget>
class QLabel;
class QHBoxLayout;
class QSlider;

//Video timeline slider. Includes the slider itself,
//plus 2 labels for video position and duration.
class TheVideoslider : public QWidget
{
    Q_OBJECT
public:
    TheVideoslider();

    void setVideoDuration(int);
    void setElapsedTime(int);

signals:
    void changeTime(qint64);

private:
    void createWidgets();
    void arrangeWidgets();
    void makeConnections();

    QLabel* label1;
    QLabel* label2;
    QSlider* slider;

private slots:
    void vSliderMoved(int);
    void videoDurationChange(qint64);
    void videoPositionChange(qint64);
    void resetTimeline();
    void resetForSameVideo();
};

#endif // THE_VIDEOSLIDER_H
