#ifndef VOLUMECONTROL_H
#define VOLUMECONTROL_H

#include <QWidget>
#include <QSlider>
#include <QHBoxLayout>
#include <QPushButton>
//volume controller, slider and mute button combined
class VolumeControl:public QWidget{
    Q_OBJECT
public:
    VolumeControl(){
        //create components
        slider= new QSlider(Qt::Horizontal);
        button= new QPushButton();
        //setup sizing
        setMinimumSize(150,30);
        setMaximumSize(250,50);
        button->setMaximumSize(30,30);
        button->setMaximumSize(50,50);

        //load icons
        muteIcon= new QIcon(":/icons/mutedbutton.png");
        unmuteIcon= new QIcon(":/icons/unmutedbutton.png");
        button->setIcon(*muteIcon);

        //create layout
        QHBoxLayout *layout=new QHBoxLayout();
        layout->addWidget(button);
        layout->addWidget(slider);
        layout->setMargin(0);//line up with video player
        layout->setSpacing(5);
        setLayout(layout);
        //link with component's signals
        connect(slider, SIGNAL(valueChanged(int)), this, SLOT(changeValue(int)));
        connect(button, SIGNAL(clicked(void)), this, SLOT(toggleMute(void)));
    }
    VolumeControl(QWidget *parent):QWidget(parent){
        VolumeControl();
    }
    //implement to keep ratio
    void resizeEvent(QResizeEvent *event);
private:
    QIcon *muteIcon,*unmuteIcon;
    QSlider *slider;
    QPushButton *button;
    bool muted=true;
    int lastVolume=50;//last non-zero volume

    //set view icon to reflect internal state
    void updateIcon();
private slots:
    void changeValue(int);
    void toggleMute();
signals:
    void changeVolume(int);
    void changeMute(bool);
};

#endif // VOLUMECONTROL_H
