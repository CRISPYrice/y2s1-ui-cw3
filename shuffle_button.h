#ifndef SHUFFLE_BUTTON_H
#define SHUFFLE_BUTTON_H

#include <QPushButton>

class ShuffleButton : public QPushButton {
Q_OBJECT
private:
public:
    ShuffleButton();

    void resizeEvent(QResizeEvent *event);

private slots:
    void clicked();
signals:
    void shuffleVideos();
};

#endif