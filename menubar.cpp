#include "menubar.h"
#include "the_button.h"
#include <QtWidgets>
#include <QImageReader>
#include <QFile>
#include <QUrl>
#include <QIcon>

//menu bar constructor
Menubar::Menubar() {
    createWidgets();
    arrangeWidgets();
    connectWidgets();
}

//Dyanmically allocates new widgets
void Menubar::createWidgets() {
    openFileBtn = new QPushButton("Open File");
}

//Arranges widgets into a QHBoxLayout
void Menubar::arrangeWidgets() {
   QHBoxLayout* layout = new QHBoxLayout();
   layout->addWidget(openFileBtn);
   layout->addStretch(1);

   setLayout(layout);
}

//Connects the open file clicked() signal to this openFile slot
void Menubar::connectWidgets() {
    connect(openFileBtn, SIGNAL(clicked()), this, SLOT(openFile()));
}

//Allows a user to import a video into the player via their file explorer
void Menubar::openFile() {
    QFileDialog fDialog(this);

    fDialog.setAcceptMode(QFileDialog::AcceptOpen);
    fDialog.setWindowTitle(tr("Open Files"));
    fDialog.setDirectory(QStandardPaths::standardLocations(QStandardPaths::MoviesLocation).value(0, QDir::homePath()));

    if (fDialog.exec() == QDialog::Accepted) {
        QString thumb = fDialog.selectedFiles().first();
        if (QFile(thumb).exists()) {
            QImageReader *imageReader = new QImageReader(thumb);
            QImage sprite = imageReader->read();
            QIcon* ico = new QIcon(QPixmap::fromImage(sprite));
            QUrl* url = new QUrl(QUrl::fromLocalFile( thumb ));
            newVideoButtonInfo = new TheButtonInfo(url, ico);
            emit openNewVideoURL(newVideoButtonInfo);

        }
    }
}
